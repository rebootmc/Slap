package net.geekxboy.playerslap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.sk89q.worldguard.protection.ApplicableRegionSet;

public class CommandHandler implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Slap.getInstance().format(Slap.getInstance().getConfig().getString("Messages.No Console")));
			return true;
		}
        final Player player = (Player) sender;
            
		if (!sender.hasPermission("slap.use")) {
			sender.sendMessage(Slap.getInstance().format(Slap.getInstance().getConfig().getString("Messages.No Permission")));
			return true;
		}
		if (args.length == 1) {
			String playerName = args[0];
			Player slapping = Bukkit.getPlayer(playerName);
			if (slapping == null) {
				sender.sendMessage(Slap.getInstance().format(Slap.getInstance().getConfig().getString("Messages.Bad Player").replace("<player>", playerName)));
				return true;
			} else {
				if (Slap.getInstance().inBlockedRegion(player)) {
					sender.sendMessage(Slap.getInstance().format(Slap.getInstance().getConfig().getString("Messages.Bad Region.Slapper").replace("<player>", playerName)));
					return true;
				} else {
					if (Slap.getInstance().inBlockedRegion(slapping)) {
						sender.sendMessage(Slap.getInstance().format(Slap.getInstance().getConfig().getString("Messages.Bad Region.Slapped").replace("<player>", playerName)));
						return true;
					} else {
						Vector unitVector = slapping.getLocation().getDirection().multiply(-1);
						slapping.setVelocity(unitVector.multiply(Slap.getInstance().getConfig().getDouble("Knockback")));
						
						sender.sendMessage(Slap.getInstance().format(Slap.getInstance().getConfig().getString("Messages.Player Slapped").replace("<player>", playerName)));
						slapping.sendMessage(Slap.getInstance().format(Slap.getInstance().getConfig().getString("Messages.Got Slapped").replace("<player>", player.getName())));
					}
				}
			}
		} else {
			sender.sendMessage(ChatColor.GRAY + "Player Slapper - Help");
			sender.sendMessage(ChatColor.GRAY + "/slap [name] - Slap a player");
			return true;
		}

		return true;
	}
}
