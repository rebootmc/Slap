package net.geekxboy.playerslap;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Slap extends JavaPlugin {
	// Plugin
	public Logger log = Logger.getLogger("Minecraft");
	public static Slap instance = null;

	// Get Config
	public FileConfiguration config;

	public void setupConfig() {
		this.config = getConfig();
		config.options().copyDefaults(true);
		saveConfig();
	}

	public void onEnable() {
		this.log = this.getLogger();
		this.log.info(this.getDescription().getFullName() + " is now enabled.");
		instance = this;

		//PluginManager pm = getServer().getPluginManager();
		//pm.registerEvents(new PlayerListener(), this);

		setupConfig();
		
		getCommand("slap").setExecutor(new CommandHandler());
	}

	public void onDisable() {
		this.log = this.getLogger();
		this.log.info(this.getDescription().getFullName() + " is now disabled.");
	}
	
	public static Slap getInstance() {
		return instance;
	}
	
	public String colorize(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}

	public String format(String str) {
		return colorize(getConfig().getString("Messages.Prefix") + str);
	}

	public void sendMessage(Player player, String message) {
		player.sendMessage(format(getConfig().getString("Messages." + message)));
	}
	
	public static WorldGuardPlugin getWorldGuard() {
		Plugin plugin = Slap.getInstance().getServer().getPluginManager().getPlugin("WorldGuard");

		if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
			return null;
		}

		return (WorldGuardPlugin) plugin;
	}
	
	public boolean inBlockedRegion(Player player) {
		ApplicableRegionSet regions = Slap.getWorldGuard().getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation());
		for (ProtectedRegion region : regions) {
			for (String blocked : Slap.getInstance().getConfig().getStringList("Blocked Regions")) {
				if (blocked.equalsIgnoreCase(region.getId())) {
					return true;
				}
			}
		}
		return false;
	}
}
